package org.bitbucket.sai92.propload;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

//TODO add javadocs
//TODO probably need to log errors not in such way (System.err)
public class EntityLoader {
	public static final String DEFAULT_TYPE = "type";

	private Map<String, Class<?>> types;
	private Map<String, Set<Object>> data;

	private String typeProperty;

	public EntityLoader() {
		types = new HashMap<>();
		data = new HashMap<>();

		typeProperty = DEFAULT_TYPE;
	}

	public String getTypeProperty() {
		return typeProperty;
	}

	public void setTypeProperty(String typeProperty) {
		this.typeProperty = typeProperty;
	}

	// types section

	public void loadTypes(String fileName) throws IOException {
		loadTypes(new File(fileName));
	}

	public void loadTypes(File file) throws IOException {
		loadTypes(createStream(file));
	}

	public void loadTypes(InputStream inStream) throws IOException {
		Properties properties = new Properties();
		properties.load(inStream);

		for (String key : properties.stringPropertyNames()) {
			addType(key, properties.getProperty(key));
		}
	}

	public void addType(String type, String fullClassName) {
		try {
			Class<?> clazz = Class.forName(fullClassName);
			addType(type, clazz);
		} catch (ClassNotFoundException e) {
			System.err.println("Class not found for type " + type + ", full class name: "
					+ fullClassName + ", details:\n" + e);
		}
	}

	public void addType(String type, Class<?> clazz) {
		types.put(type, clazz);
	}

	public void setTypes(Map<String, Class<?>> types) {
		if (types == null) {
			throw new NullPointerException("Types map cannot be null!");
		}

		this.types = types;
	}

	public Map<String, Class<?>> getTypes() {
		return types;
	}

	// data section

	public Map<String, Set<Object>> getData() {
		return data;
	}

	public void loadAllData(String fileName) throws IOException {
		loadAllData(new File(fileName));
	}

	public void loadAllData(File file) throws IOException {
		if (file.isFile()) {
			loadData(file);
			return;
		}

		File[] innerFiles = file.listFiles();
		if (innerFiles == null) {
			return;
		}

		for (File innerFile : innerFiles) {
			loadAllData(innerFile);
		}
	}

	public void loadData(String fileName) throws IOException {
		loadData(new File(fileName));
	}

	public void loadData(File file) throws IOException {
		loadData(createStream(file));
	}

	public void loadData(InputStream inStream) throws IOException {
		Properties properties = new Properties();
		properties.load(inStream);

		addEntity(properties);
	}

	protected void addEntity(Properties properties) {
		String type = properties.getProperty(typeProperty);
		Class<?> clazz = types.get(type);

		if (clazz == null) {
			System.err.println("Class not loaded for type " + type + ", cannot load entity.");
			return;
		}

		try {
			Object instance = clazz.newInstance();

			fillEntity(properties, clazz, instance);
			addEntity(type, instance);
		} catch (InstantiationException | IllegalAccessException e) {
			System.err.println("Instance of class " + clazz + " cannot be created, details:\n" + e);
		}

	}

	protected void fillEntity(Properties properties, Class<?> clazz, Object instance) {
		for (String key : properties.stringPropertyNames()) {
			if (key.equals(typeProperty)) {
				continue;
			}

			Field field = null;
			try {
				String value = properties.getProperty(key);

				field = getField(clazz, key);
				field.setAccessible(true);

				Class<?> type = field.getType();

				if (type == String.class) {
					field.set(instance, value);
					continue;
				}

				if (type == Integer.class || type == int.class) {
					field.set(instance, Integer.parseInt(value));
					continue;
				}

				if (type == Boolean.class || type == boolean.class) {
					field.set(instance, value.equals("1") || value.equalsIgnoreCase("true"));
					continue;
				}

				// TODO add double and char

				throw new IllegalArgumentException(
						"Cannot convert type " + type + " of field " + key + ", class: " + clazz);
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException
					| IllegalAccessException e) {
				System.err.println(
						"Cannot get/set field " + key + " of " + clazz + ", details:\n" + e);
			} catch (NullPointerException e) {
				System.err.println("NPE on getting/setting field " + key + " of " + clazz
						+ ", stack trace:\n" + e.getStackTrace());
			} finally {
				if (field != null) {
					field.setAccessible(false);
				}
			}
		}

	}

	protected void addEntity(String type, Object instance) {
		Set<Object> dataSet = data.get(type);

		if (dataSet == null) {
			dataSet = new HashSet<>();
			data.put(type, dataSet);
		}

		dataSet.add(instance);
	}

	private Field getField(Class<?> clazz, String fieldName) throws NoSuchFieldException {
		Field field = getFieldRecursivelyFromThisOrSuperClass(clazz, fieldName);

		if (field == null) {
			throw new NoSuchFieldException(
					"Field " + fieldName + " was not found in " + clazz + " and superclasses.");
		}

		return field;
	}

	private static Field getFieldRecursivelyFromThisOrSuperClass(Class<?> clazz, String fieldName) {
		Field field = null;

		try {
			field = clazz.getDeclaredField(fieldName);
		} catch (NoSuchFieldException e) {
			Class<?> superClazz = clazz.getSuperclass();

			if (superClazz != null) {
				return getFieldRecursivelyFromThisOrSuperClass(superClazz, fieldName);
			}
		}

		return field;
	}

	private static InputStream createStream(File file) throws FileNotFoundException {
		return new BufferedInputStream(new FileInputStream(file));
	}
}
